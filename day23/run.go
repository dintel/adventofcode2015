package day23

import (
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

type instruction struct {
	name     string
	register *int
	offset   int
}

func (i *instruction) execute() int {
	switch i.name {
	case "hlf":
		*i.register /= 2
	case "tpl":
		*i.register *= 3
	case "inc":
		*i.register += 1
	case "jmp":
		return i.offset
	case "jie":
		if *i.register%2 == 0 {
			return i.offset
		}
	case "jio":
		if *i.register == 1 {
			return i.offset
		}
	default:
		log.Fatalf("Unknown instruction %s", i.name)
	}
	return 0
}

type computer struct {
	regA         int
	regB         int
	instructions []instruction
}

func NewComputer() *computer {
	return &computer{
		regA:         0,
		regB:         0,
		instructions: make([]instruction, 0),
	}
}

func (c *computer) AddInstruction(name, register string, offset int) {
	i := instruction{
		name:     name,
		register: nil,
		offset:   offset,
	}
	switch register {
	case "a":
		i.register = &c.regA
	case "b":
		i.register = &c.regB
	case "":
	}
	c.instructions = append(c.instructions, i)
}

func (c *computer) Run() {
	pc := 0
	for pc < len(c.instructions) && pc >= 0 {
		offset := c.instructions[pc].execute()
		if offset == 0 {
			pc++
		} else {
			pc += offset
		}
	}
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	c := NewComputer()
	for _, line := range lines {
		parts := strings.Split(line, " ")
		parts[1] = strings.TrimSuffix(parts[1], ",")
		if parts[0] == "hlf" || parts[0] == "tpl" || parts[0] == "inc" {
			c.AddInstruction(parts[0], parts[1], 0)
		} else if parts[0] == "jmp" {
			offset, err := strconv.Atoi(parts[1])
			if err != nil {
				log.Fatalf("Failed parsing number %s", parts[1])
			}
			c.AddInstruction(parts[0], "", offset)
		} else if parts[0] == "jie" || parts[0] == "jio" {
			offset, err := strconv.Atoi(parts[2])
			if err != nil {
				log.Fatalf("Failed parsing number %s", parts[1])
			}
			c.AddInstruction(parts[0], parts[1], offset)
		} else {
			log.Fatalf("Unknown instruction while parsing line %s", line)
		}
	}

	switch part {
	case 1:
		c.Run()
		log.Printf("Value of register b is %d", c.regB)
	case 2:
		c.regA = 1
		c.Run()
		log.Printf("Value of register b is %d", c.regB)
	}
}
