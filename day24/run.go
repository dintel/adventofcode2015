package day24

import (
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

func clonePresents(presents []int, except int) []int {
	result := make([]int, len(presents)-1)
	j := 0
	for i, present := range presents {
		if i != except {
			result[j] = present
			j++
		}
	}
	return result
}

func firstCombination(presents []int, length int, sum int) [][]int {
	result := make([][]int, 0)
	if sum < 0 {
		return result
	}
	if length != 0 {
		for i, p := range presents {
			newPresents := clonePresents(presents, i)
			parts := firstCombination(newPresents, length-1, sum-p)
			for _, part := range parts {
				result = append(result, append(part, p))
			}
			if len(result) != 0 {
				break
			}
		}
	} else {
		if sum == 0 {
			result = append(result, make([]int, 0))
		}
	}
	return result
}

func sliceSum(presents []int) int {
	result := 0
	for _, n := range presents {
		result += n
	}
	return result
}

func quantumEntanglement(presents []int) int {
	result := presents[0]
	for i := 1; i < len(presents); i++ {
		result *= presents[i]
	}
	return result
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	presents := make([]int, len(lines))
	total := 0
	for i, line := range lines {
		num, err := strconv.Atoi(line)
		if err != nil {
			log.Fatalf("Failed parsing line %s", line)
		}
		presents[i] = num
		total += num
	}
	log.Printf("There are %d presents, using %d space in total", len(presents), total)

	switch part {
	case 1:
		for i := 1; i < len(presents)/2; i++ {
			c := firstCombination(presents, i, total/3)
			if len(c) != 0 {
				log.Printf("Quantum entanglement of best configuration is %d", quantumEntanglement(c[0]))
				break
			}
		}
	case 2:
		for i := 1; i < len(presents)/2; i++ {
			c := firstCombination(presents, i, total/4)
			if len(c) != 0 {
				log.Printf("Quantum entanglement of best configuration is %d", quantumEntanglement(c[0]))
				break
			}
		}
	}
}
