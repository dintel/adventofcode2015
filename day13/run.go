package day13

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type guest struct {
	Name         string
	happinessMap map[string]int
}

func NewGuest(name string) *guest {
	return &guest{
		Name:         name,
		happinessMap: make(map[string]int),
	}
}

func (g *guest) SetHappinessMapEntry(name string, happiness int) {
	g.happinessMap[name] = happiness
}

func (g *guest) Happiness(left, right *guest) int {
	return g.happinessMap[left.Name] + g.happinessMap[right.Name]
}

func (g *guest) String() string {
	return g.Name
}

type seating []*guest

func (s seating) Clone() seating {
	clone := make(seating, len(s))
	copy(clone, s)
	return clone
}

func (s seating) Next(n int) *guest {
	n++
	if n > len(s)-1 {
		n = 0
	}
	return s[n]
}

func (s seating) Prev(n int) *guest {
	n--
	if n < 0 {
		n = len(s) - 1
	}
	return s[n]
}

func (s seating) Happiness() int {
	happiness := 0
	for i := 0; i < len(s); i++ {
		happiness += s[i].Happiness(s.Prev(i), s.Next(i))
	}
	return happiness
}

func FindBestSeating(guests []*guest) (seating, int) {
	s := make([]*guest, len(guests))
	return FindBestSeatingRecursive(guests, s)
}

func FindBestSeatingRecursive(guests []*guest, s seating) (seating, int) {
	if len(guests) == 0 {
		return s, s.Happiness()
	}
	guest := guests[0]
	newGuests := guests[1:]
	bestSeating := s
	bestHappiness := 0
	for i := 0; i < len(s); i++ {
		if s[i] == nil {
			s[i] = guest
			newSeating, newHappiness := FindBestSeatingRecursive(newGuests, s)
			if newHappiness > bestHappiness {
				bestHappiness = newHappiness
				bestSeating = newSeating.Clone()
			}
			s[i] = nil
		}
	}
	return bestSeating, bestHappiness
}

func (s seating) String() string {
	result := "\n"
	for i := 0; i < len(s); i++ {
		result += fmt.Sprintf("%d: %s\n", i, s[i])
	}
	return result
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	guestsMap := make(map[string]*guest)
	guests := make([]*guest, 0)
	for _, line := range lines {
		var person, action, next string
		var happiness int
		_, err := fmt.Sscanf(line, "%s would %s %d happiness units by sitting next to %s", &person, &action, &happiness, &next)
		next = strings.TrimSuffix(next, ".")
		if err != nil {
			log.Fatalf("Failed parsing line: %s (%s)", line, err)
		}
		if action == "lose" {
			happiness = -happiness
		}
		guest, ok := guestsMap[person]
		if !ok {
			guest = NewGuest(person)
			guestsMap[person] = guest
			guests = append(guests, guest)
		}
		guest.SetHappinessMapEntry(next, happiness)
	}

	switch part {
	case 1:
		bestSeating, bestHappiness := FindBestSeating(guests)
		log.Print(bestSeating)
		log.Printf("Best seating found has a total of %d happiness", bestHappiness)
	case 2:
		me := NewGuest("Santa")
		for _, guest := range guests {
			guest.SetHappinessMapEntry(me.Name, 0)
			me.SetHappinessMapEntry(guest.Name, 0)
		}
		guests = append(guests, me)
		bestSeating, bestHappiness := FindBestSeating(guests)
		log.Print(bestSeating)
		log.Printf("Best seating found has a total of %d happiness", bestHappiness)
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
