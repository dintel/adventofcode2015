package day7

import (
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

type gate interface {
	Value() uint16
	Resolve() bool
	Unresolve()
	Override(uint16)
}

type gateStatic struct {
	StaticValue uint16
}

func (g *gateStatic) Value() uint16 {
	return g.StaticValue
}

func (g *gateStatic) Unresolve() {}

func (g *gateStatic) Resolve() bool {
	return true
}

func (g *gateStatic) Override(value uint16) {
	g.StaticValue = value
}

type gateCopy struct {
	v        uint16
	resolved bool
	Source   gate
}

func (g *gateCopy) Value() uint16 {
	return g.v
}

func (g *gateCopy) Unresolve() {
	g.resolved = false
}

func (g *gateCopy) Resolve() bool {
	if g.Source == nil {
		log.Print("gateCopy.Resolve - source is nil")
		return false
	}
	if g.resolved {
		return true
	}
	if g.Source.Resolve() {
		g.v = g.Source.Value()
		g.resolved = true
		return true
	}
	return false
}

func (g *gateCopy) Override(value uint16) {
	g.v = value
}

type gateNot struct {
	v        uint16
	resolved bool
	Source   gate
}

func (g *gateNot) Value() uint16 {
	return g.v
}

func (g *gateNot) Unresolve() {
	g.resolved = false
}

func (g *gateNot) Resolve() bool {
	if g.Source == nil {
		return false
	}
	if g.resolved {
		return true
	}
	if g.Source.Resolve() {
		g.v = ^g.Source.Value()
		g.resolved = true
		return true
	}
	return false
}

func (g *gateNot) Override(value uint16) {
	g.v = value
}

type gateAnd struct {
	v        uint16
	resolved bool
	Left     gate
	Right    gate
}

func (g *gateAnd) Value() uint16 {
	return g.v
}

func (g *gateAnd) Unresolve() {
	g.resolved = false
}

func (g *gateAnd) Resolve() bool {
	if g.Left == nil || g.Right == nil {
		return false
	}
	if g.resolved {
		return true
	}
	if g.Left.Resolve() && g.Right.Resolve() {
		g.v = g.Left.Value() & g.Right.Value()
		g.resolved = true
		return true
	}
	return false
}

func (g *gateAnd) Override(value uint16) {
	g.v = value
}

type gateOr struct {
	v        uint16
	resolved bool
	Left     gate
	Right    gate
}

func (g *gateOr) Value() uint16 {
	return g.v
}

func (g *gateOr) Unresolve() {
	g.resolved = false
}

func (g *gateOr) Resolve() bool {
	if g.Left == nil || g.Right == nil {
		return false
	}
	if g.resolved {
		return true
	}
	if g.Left.Resolve() && g.Right.Resolve() {
		g.v = g.Left.Value() | g.Right.Value()
		g.resolved = true
		return true
	}
	return false
}

func (g *gateOr) Override(value uint16) {
	g.v = value
}

type gateLshift struct {
	v        uint16
	resolved bool
	Source   gate
	By       uint
}

func (g *gateLshift) Value() uint16 {
	return g.v
}

func (g *gateLshift) Unresolve() {
	g.resolved = false
}

func (g *gateLshift) Resolve() bool {
	if g.Source == nil {
		return false
	}
	if g.resolved {
		return true
	}
	if g.Source.Resolve() {
		g.v = g.Source.Value() << g.By
		g.resolved = true
		return true
	}
	return false
}

func (g *gateLshift) Override(value uint16) {
	g.v = value
}

type gateRshift struct {
	v        uint16
	resolved bool
	Source   gate
	By       uint
}

func (g *gateRshift) Value() uint16 {
	return g.v
}

func (g *gateRshift) Unresolve() {
	g.resolved = false
}

func (g *gateRshift) Resolve() bool {
	if g.Source == nil {
		return false
	}
	if g.resolved {
		return true
	}
	if g.Source.Resolve() {
		g.v = g.Source.Value() >> g.By
		g.resolved = true
		return true
	}
	return false
}

func (g *gateRshift) Override(value uint16) {
	g.v = value
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	wires := make(map[string]gate)

	for _, line := range lines {
		lexems := strings.Split(line, " ")
		switch len(lexems) {
		case 3:
			num, err := strconv.Atoi(lexems[0])
			if err == nil {
				wires[lexems[2]] = &gateStatic{StaticValue: uint16(num)}
			} else {
				wires[lexems[2]] = &gateCopy{Source: wires[lexems[0]]}
			}
		case 4:
			wires[lexems[3]] = &gateNot{Source: wires[lexems[1]]}
		case 5:
			switch lexems[1] {
			case "AND":
				left := wires[lexems[0]]
				leftNum, err := strconv.Atoi(lexems[0])
				if err == nil {
					left = &gateStatic{StaticValue: uint16(leftNum)}
				}
				right := wires[lexems[2]]
				rightNum, err := strconv.Atoi(lexems[2])
				if err == nil {
					right = &gateStatic{StaticValue: uint16(rightNum)}
				}
				wires[lexems[4]] = &gateAnd{Left: left, Right: right}
			case "OR":
				left := wires[lexems[0]]
				leftNum, err := strconv.Atoi(lexems[0])
				if err == nil {
					left = &gateStatic{StaticValue: uint16(leftNum)}
				}
				right := wires[lexems[2]]
				rightNum, err := strconv.Atoi(lexems[2])
				if err == nil {
					right = &gateStatic{StaticValue: uint16(rightNum)}
				}
				wires[lexems[4]] = &gateOr{Left: left, Right: right}
			case "LSHIFT":
				by, err := strconv.Atoi(lexems[2])
				if err != nil {
					log.Fatalf("Failed parsing number %s in line - %s", lexems[2], line)
				}
				wires[lexems[4]] = &gateLshift{Source: wires[lexems[0]], By: uint(by)}
			case "RSHIFT":
				by, err := strconv.Atoi(lexems[2])
				if err != nil {
					log.Fatalf("Failed parsing number %s in line - %s", lexems[2], line)
				}
				wires[lexems[4]] = &gateRshift{Source: wires[lexems[0]], By: uint(by)}
			}
		default:
			log.Fatalf("Failed parsing line - %s", line)
		}
	}

	for _, line := range lines {
		lexems := strings.Split(line, " ")
		switch len(lexems) {
		case 3:
			_, err := strconv.Atoi(lexems[0])
			if err != nil {
				if wires[lexems[2]].(*gateCopy).Source == nil {
					wires[lexems[2]].(*gateCopy).Source = wires[lexems[0]]
				}
			}
		case 4:
			if wires[lexems[3]].(*gateNot).Source == nil {
				wires[lexems[3]].(*gateNot).Source = wires[lexems[1]]
			}
		case 5:
			switch lexems[1] {
			case "AND":
				if wires[lexems[4]].(*gateAnd).Left == nil {
					wires[lexems[4]].(*gateAnd).Left = wires[lexems[0]]
				}
				if wires[lexems[4]].(*gateAnd).Right == nil {
					wires[lexems[4]].(*gateAnd).Right = wires[lexems[2]]
				}
			case "OR":
				if wires[lexems[4]].(*gateOr).Left == nil {
					wires[lexems[4]].(*gateOr).Left = wires[lexems[0]]
				}
				if wires[lexems[4]].(*gateOr).Right == nil {
					wires[lexems[4]].(*gateOr).Right = wires[lexems[2]]
				}
			case "LSHIFT":
				if wires[lexems[4]].(*gateLshift).Source == nil {
					wires[lexems[4]].(*gateLshift).Source = wires[lexems[0]]
				}
			case "RSHIFT":
				if wires[lexems[4]].(*gateRshift).Source == nil {
					wires[lexems[4]].(*gateRshift).Source = wires[lexems[0]]
				}
			}
		default:
			log.Fatalf("Failed parsing line - %s", line)
		}
	}

	wireNames := make([]string, 0)
	for name, _ := range wires {
		wireNames = append(wireNames, name)
	}
	sort.Strings(wireNames)

	switch part {
	case 1:
		wires["a"].Resolve()
		log.Printf("Wire a value %d", wires["a"].Value())
	case 2:
		wires["a"].Resolve()
		log.Printf("Wire a value %d", wires["a"].Value())
		wires["b"].Override(wires["a"].Value())
		log.Printf("Overridden wire b to value %d", wires["b"].Value())
		for _, wire := range wires {
			wire.Unresolve()
		}
		wires["a"].Resolve()
		log.Printf("Wire a value %d", wires["a"].Value())
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
