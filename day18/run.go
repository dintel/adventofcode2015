package day18

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type grid struct {
	lights [][]bool
	stuck  [][]int
}

func newGrid(width, height int) *grid {
	lights := make([][]bool, width)
	for i := 0; i < width; i++ {
		lights[i] = make([]bool, height)
	}
	return &grid{
		lights: lights,
		stuck:  make([][]int, 0),
	}
}

func (g *grid) Set(x, y int, state bool) {
	g.lights[x][y] = state
}

func (g *grid) Neighbors(x, y int, state bool) int {
	result := 0
	for i := x - 1; i <= x+1; i++ {
		for j := y - 1; j <= y+1; j++ {
			if i == x && j == y {
				continue
			}
			if g.Get(i, j) == state {
				result++
			}
		}
	}
	return result
}

func (g *grid) Get(x, y int) bool {
	if x < 0 || x >= len(g.lights) {
		return false
	}
	if y < 0 || y >= len(g.lights[0]) {
		return false
	}
	return g.lights[x][y]
}

func (g *grid) Next(x, y int) bool {
	if g.lights[x][y] {
		on := g.Neighbors(x, y, true)
		return on == 2 || on == 3
	} else {
		on := g.Neighbors(x, y, true)
		return on == 3
	}
}

func (g *grid) Iterate() {
	newLights := make([][]bool, len(g.lights))
	for x := 0; x < len(g.lights); x++ {
		newLights[x] = make([]bool, len(g.lights[x]))
		for y := 0; y < len(g.lights[x]); y++ {
			newLights[x][y] = g.Next(x, y)
		}
	}
	for _, point := range g.stuck {
		newLights[point[0]][point[1]] = true
	}
	g.lights = newLights
}

func (g *grid) Count(state bool) int {
	result := 0
	for x := 0; x < len(g.lights); x++ {
		for y := 0; y < len(g.lights[x]); y++ {
			if g.lights[x][y] == state {
				result++
			}
		}
	}
	return result
}

func (g *grid) SetStuck(x, y int) {
	point := make([]int, 2)
	point[0], point[1] = x, y
	g.stuck = append(g.stuck, point)
	g.lights[x][y] = true
}

func Run(part int) {
	const iterations = 100
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	grid := newGrid(100, 100)
	for i, line := range lines {
		for j, ch := range line {
			switch ch {
			case '.':
				grid.Set(j, i, false)
			case '#':
				grid.Set(j, i, true)
			default:
				log.Fatalf("Failed parsing character '%c'", ch)
			}
		}
	}

	switch part {
	case 1:
		for i := 0; i < iterations; i++ {
			grid.Iterate()
		}
		log.Printf("There are %d lights on after %d iterations", grid.Count(true), iterations)
	case 2:
		grid.SetStuck(0, 0)
		grid.SetStuck(0, 99)
		grid.SetStuck(99, 0)
		grid.SetStuck(99, 99)
		for i := 0; i < iterations; i++ {
			grid.Iterate()
		}
		log.Printf("There are %d lights on after %d iterations", grid.Count(true), iterations)
	}
}
