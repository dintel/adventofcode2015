package day4

import (
	"crypto/md5"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	h := md5.New()

	switch part {
	case 1:
		i := 1
		for {
			result := fmt.Sprintf("%s%d", data, i)
			_, err := io.WriteString(h, result)
			if err != nil {
				log.Fatalf("Error - %s", err)
			}
			sum := fmt.Sprintf("%x", h.Sum(nil))
			if strings.HasPrefix(sum, "00000") {
				log.Printf("Found string %s with checksum %s", result, sum)
				log.Printf("Checked %d hashes", i)
				return
			}
			h.Reset()
			i++
		}
	case 2:
		i := 1
		for {
			result := fmt.Sprintf("%s%d", data, i)
			_, err := io.WriteString(h, result)
			if err != nil {
				log.Fatalf("Error - %s", err)
			}
			sum := fmt.Sprintf("%x", h.Sum(nil))
			if strings.HasPrefix(sum, "000000") {
				log.Printf("Found string %s with checksum %s", result, sum)
				log.Printf("Checked %d hashes", i)
				return
			}
			h.Reset()
			i++
		}
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
