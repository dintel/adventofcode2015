package day25

import (
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strconv"
)

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	r, err := regexp.Compile(`row (\d+).+column (\d+)`)
	if err != nil {
		log.Fatalf("Failed to compile regexp - %s", err)
	}
	matches := r.FindStringSubmatch(string(data))
	rowString := matches[1]
	columnString := matches[2]
	row, err := strconv.Atoi(rowString)
	if err != nil {
		log.Fatalf("Failed psring number %s - %s", rowString, err)
	}
	column, err := strconv.Atoi(columnString)
	if err != nil {
		log.Fatalf("Failed psring number %s - %s", columnString, err)
	}

	const firstCode = 20151125
	const multiplier = 252533
	const divisor = 33554393
	switch part {
	case 1:
		n := 1
		for i := 1; i < row; i++ {
			n += i
		}
		for i := 1; i < column; i++ {
			n += i + row
		}
		code := firstCode
		for i := 1; i < n; i++ {
			code *= multiplier
			code = code % divisor
		}
		log.Print(code)
	case 2:
		log.Print("Just start the weather mahine")
	}
}
