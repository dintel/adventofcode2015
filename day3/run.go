package day3

import (
	"io/ioutil"
	"log"
	"os"
)

type houses struct {
	Houses map[int][]int
	Total  int
}

func (h *houses) Visit(x, y int) {
	for _, curY := range h.Houses[x] {
		if curY == y {
			return
		}
	}
	h.Houses[x] = append(h.Houses[x], y)
	h.Total++
}

type house struct {
	x, y int
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}

	switch part {
	case 1:
		houses := &houses{Houses: make(map[int][]int)}
		x, y := 0, 0
		houses.Visit(x, y)
		for _, c := range data {
			switch c {
			case '^':
				y++
			case 'v':
				y--
			case '>':
				x++
			case '<':
				x--
			default:
				log.Printf("Unknown character %s", string(c))
			}
			houses.Visit(x, y)
		}
		log.Printf("Made %d moves", len(data))
		log.Printf("Visited %d houses", houses.Total)
	case 2:
		houses := &houses{Houses: make(map[int][]int)}
		x, y := 0, 0
		robox, roboy := 0, 0
		houses.Visit(x, y)
		for i, c := range data {
			if i%2 == 0 {
				switch c {
				case '^':
					y++
				case 'v':
					y--
				case '>':
					x++
				case '<':
					x--
				default:
					log.Printf("Unknown character %s", string(c))
				}
				houses.Visit(x, y)
			} else {
				switch c {
				case '^':
					roboy++
				case 'v':
					roboy--
				case '>':
					robox++
				case '<':
					robox--
				default:
					log.Printf("Unknown character %s", string(c))
				}
				houses.Visit(robox, roboy)
			}
		}
		log.Printf("Santa and Robo-Santa made %d moves", len(data))
		log.Printf("Total visited %d houses", houses.Total)
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
