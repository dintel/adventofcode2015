package day14

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type reindeer struct {
	Name     string
	Speed    int
	FlyTime  int
	RestTime int
}

func (r *reindeer) String() string {
	return fmt.Sprintf("%s flies at %d km/s for %d seconds, then rests for %d seconds", r.Name, r.Speed, r.FlyTime, r.RestTime)
}

func (r *reindeer) Position(time int) int {
	cycle := r.FlyTime + r.RestTime
	result := r.Speed * r.FlyTime * (time / cycle)
	time = time % cycle
	if time > r.FlyTime {
		result += r.FlyTime * r.Speed
	} else {
		result += time * r.Speed
	}
	return result
}

func Run(part int) {
	const seconds = 2503
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	reindeers := make([]*reindeer, 0)
	for _, line := range lines {
		var name string
		var speed, flyTime, restTime int
		_, err := fmt.Sscanf(line, "%s can fly %d km/s for %d seconds, but then must rest for %d seconds.", &name, &speed, &flyTime, &restTime)
		if err != nil {
			log.Fatalf("Failed parsing line '%s' (%s)", line, err)
		}
		reindeer := &reindeer{
			Name:     name,
			Speed:    speed,
			FlyTime:  flyTime,
			RestTime: restTime,
		}
		reindeers = append(reindeers, reindeer)
	}

	switch part {
	case 1:
		bestPosition := 0
		bestReindeer := (*reindeer)(nil)
		for _, r := range reindeers {
			position := r.Position(seconds)
			if position > bestPosition {
				bestPosition = position
				bestReindeer = r
			}
		}
		log.Printf("Winner is %s with travel distance %d", bestReindeer.Name, bestPosition)
	case 2:
		points := make(map[*reindeer]int)
		positions := make(map[*reindeer]int)
		for i := 1; i <= seconds; i++ {
			leaderPosition := 0
			for _, r := range reindeers {
				positions[r] = r.Position(i)
				if positions[r] > leaderPosition {
					leaderPosition = positions[r]
				}
			}
			for r, p := range positions {
				if p == leaderPosition {
					points[r]++
				}
			}
		}
		maxPoints := 0
		for _, p := range points {
			if p > maxPoints {
				maxPoints = p
			}
		}
		log.Printf("Winning reindeer has %d points", maxPoints)
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
