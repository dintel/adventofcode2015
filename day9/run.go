package day9

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func deleteString(slice []string, str string) []string {
	index := -1
	for i, v := range slice {
		if v == str {
			index = i
		}
	}
	if index != -1 {
		slice = append(slice[:index], slice[index+1:]...)
	}
	return slice
}

type vertex struct {
	Name     string
	Visited  bool
	SPLength int
	SPPrev   *vertex
	Adjacent map[string]int
}

type graph struct {
	Vertexes []*vertex
	nameMap  map[string]*vertex
}

func NewGraph() *graph {
	return &graph{
		Vertexes: make([]*vertex, 0),
		nameMap:  make(map[string]*vertex),
	}
}

func (g *graph) HasVertex(name string) bool {
	for _, v := range g.Vertexes {
		if v.Name == name {
			return true
		}
	}
	return false
}

func (g *graph) AddVertex(name string) {
	v := &vertex{
		Name:     name,
		Adjacent: make(map[string]int),
	}
	g.Vertexes = append(g.Vertexes, v)
	g.nameMap[v.Name] = v
}

func (g *graph) AddEdge(from, to string, dist int) {
	fromV, ok := g.nameMap[from]
	if !ok {
		log.Printf("Could not find vertex %s", from)
		return
	}
	toV, ok := g.nameMap[to]
	if !ok {
		log.Printf("Could not find vertex %s", to)
		return
	}
	fromV.Adjacent[to] = dist
	toV.Adjacent[from] = dist
}

func inQueue(q []*vertex, name string) bool {
	for _, v := range q {
		if v.Name == name {
			return true
		}
	}
	return false
}

func (g *graph) NotVisitedAdj(v *vertex) []*vertex {
	result := make([]*vertex, 0)
	for name, _ := range v.Adjacent {
		if !g.nameMap[name].Visited {
			result = append(result, g.nameMap[name])
		}
	}
	return result
}

func (g *graph) ComputeShortestFullPath(first int) ([]string, int) {
	for _, v := range g.Vertexes {
		v.Visited = false
	}
	current := g.Vertexes[first]
	notVisitedAdj := g.NotVisitedAdj(current)
	minDistance := 0
	path := make([]string, 1)
	path[0] = current.Name
	for len(notVisitedAdj) != 0 {
		current.Visited = true
		min := 0
		closest := ""
		for _, v := range notVisitedAdj {
			if min == 0 {
				min = current.Adjacent[v.Name]
				closest = v.Name
			}
			if current.Adjacent[v.Name] < min {
				min = current.Adjacent[v.Name]
				closest = v.Name
			}
		}
		path = append(path, closest)
		minDistance += min
		current = g.nameMap[closest]
		notVisitedAdj = g.NotVisitedAdj(current)
	}
	return path, minDistance
}

func (g *graph) ComputeLongestFullPath(first int) ([]string, int) {
	for _, v := range g.Vertexes {
		v.Visited = false
	}
	current := g.Vertexes[first]
	notVisitedAdj := g.NotVisitedAdj(current)
	maxDistance := 0
	path := make([]string, 1)
	path[0] = current.Name
	for len(notVisitedAdj) != 0 {
		current.Visited = true
		max := 0
		closest := ""
		for _, v := range notVisitedAdj {
			if current.Adjacent[v.Name] > max {
				max = current.Adjacent[v.Name]
				closest = v.Name
			}
		}
		path = append(path, closest)
		maxDistance += max
		current = g.nameMap[closest]
		notVisitedAdj = g.NotVisitedAdj(current)
	}
	return path, maxDistance
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	graph := NewGraph()
	for _, line := range lines {
		var from, to string
		var distance int
		_, err := fmt.Sscanf(line, "%s to %s = %d", &from, &to, &distance)
		if err != nil {
			log.Fatalf("Failed parsing line %s - %s", line, err)
		}
		if !graph.HasVertex(from) {
			graph.AddVertex(from)
		}
		if !graph.HasVertex(to) {
			graph.AddVertex(to)
		}
		graph.AddEdge(from, to, distance)
		graph.AddEdge(to, from, distance)
	}

	switch part {
	case 1:
		resultPath, resultMinDistance := graph.ComputeShortestFullPath(0)
		for i := 1; i < len(graph.Vertexes); i++ {
			path, minDistance := graph.ComputeShortestFullPath(i)
			if minDistance < resultMinDistance {
				resultMinDistance = minDistance
				resultPath = path
			}
		}
		log.Printf("Shortest path is %s", strings.Join(resultPath, " -> "))
		log.Printf("Shortest path length is %d", resultMinDistance)
	case 2:
		resultPath, resultMaxDistance := graph.ComputeLongestFullPath(0)
		for i := 1; i < len(graph.Vertexes); i++ {
			path, maxDistance := graph.ComputeLongestFullPath(i)
			if maxDistance > resultMaxDistance {
				resultMaxDistance = maxDistance
				resultPath = path
			}
		}
		log.Printf("Longest path is %s", strings.Join(resultPath, " -> "))
		log.Printf("Longest path length is %d", resultMaxDistance)
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
