package day20

import (
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	search, err := strconv.Atoi(string(data))
	if err != nil {
		log.Fatalf("Failed parsing input '%s' %s", string(data), err)
	}

	switch part {
	case 1:
		houses := make([]int, search/10)
		for i := 1; i < len(houses); i++ {
			for j := i; j < len(houses); j += i {
				houses[j] += i * 10
			}
		}
		for i, presents := range houses {
			if presents >= search {
				log.Printf("House number %d has %d presents delivered", i, presents)
				return
			}
		}
		log.Printf("Checked first %d houses and did not find a match")
	case 2:
		houses := make([]int, search/5)
		for i := 1; i < len(houses); i++ {
			for j := i; j < len(houses) && j <= i*50; j += i {
				houses[j] += i * 11
			}
		}
		for i, presents := range houses {
			if presents >= search {
				log.Printf("House number %d has %d presents delivered", i, presents)
				return
			}
		}
		log.Printf("Checked first %d houses and did not find a match")
	}
}
