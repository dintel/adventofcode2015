package day22

import (
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

type sim struct {
	playerHp    int
	playerArmor int
	playerMana  int
	bossHp      int
	bossDamage  int

	spells map[string]int

	shieldTimer   int
	poisonTimer   int
	rechargeTimer int

	manaUsed   int
	turn       int
	spellsUsed []string
}

func NewSim(playerHp, playerMana, bossHp, bossDamage int, spells map[string]int) sim {
	return sim{
		playerHp:    playerHp,
		playerArmor: 0,
		playerMana:  playerMana,
		bossHp:      bossHp,
		bossDamage:  bossDamage,

		spells: spells,

		shieldTimer:   -1,
		poisonTimer:   -1,
		rechargeTimer: -1,

		manaUsed:   0,
		turn:       0,
		spellsUsed: make([]string, 0),
	}
}

func (s *sim) Clone() sim {
	result := *s
	result.spellsUsed = make([]string, len(s.spellsUsed))
	copy(result.spellsUsed, s.spellsUsed)
	return result
}

func (s *sim) useSpell(name string) {
	s.manaUsed += s.spells[name]
	s.playerMana -= s.spells[name]
	s.spellsUsed = append(s.spellsUsed, name)
	switch name {
	case "Magic Missile":
		s.bossHp -= 4
	case "Drain":
		s.bossHp -= 2
		s.playerHp += 2
	case "Shield":
		s.shieldTimer = 6
		s.playerArmor = 7
	case "Poison":
		s.poisonTimer = 6
	case "Recharge":
		s.rechargeTimer = 5
	default:
		log.Fatalf("Unknown spell %s", name)
	}
}

func (s *sim) applyEffects() {
	if s.shieldTimer > 0 {
		s.shieldTimer--
	}
	if s.poisonTimer > 0 {
		s.poisonTimer--
	}
	if s.rechargeTimer > 0 {
		s.rechargeTimer--
	}

	if s.shieldTimer == 0 {
		s.playerArmor = 0
		s.shieldTimer = -1
	}
	if s.poisonTimer >= 0 {
		s.bossHp -= 3
		if s.poisonTimer == 0 {
			s.poisonTimer = -1
		}
	}
	if s.rechargeTimer >= 0 {
		s.playerMana += 101
		if s.rechargeTimer == 0 {
			s.rechargeTimer = -1
		}
	}
}

func (s *sim) bossTurn() {
	damage := s.bossDamage - s.playerArmor
	if damage < 1 {
		damage = 1
	}
	s.playerHp -= damage
}

func (s *sim) availableSpells() []string {
	result := make([]string, 0)
	for name := range s.spells {
		if s.spells[name] <= s.playerMana {
			if (name == "Shield" && s.shieldTimer == -1) || (name == "Poison" && s.poisonTimer == -1) || (name == "Recharge" && s.rechargeTimer == -1) || (name == "Magic Missile" || name == "Drain") {
				result = append(result, name)
			}
		}
	}
	return result
}

func (s *sim) won() bool {
	return s.playerHp > 0 && s.bossHp <= 0
}

func (s *sim) lost() bool {
	return s.playerHp <= 0
}

func findLeastMana(s sim, foundLeast *int, seq *[]string, hard bool) {
	s.turn++
	playerTurn := s.turn%2 == 1
	if playerTurn && hard {
		s.playerHp--
		if s.lost() {
			return
		}
	}
	s.applyEffects()
	if s.won() {
		if *foundLeast == -1 || s.manaUsed < *foundLeast {
			*foundLeast = s.manaUsed
			*seq = s.spellsUsed
		}
		return
	}
	if s.lost() {
		return
	}
	if playerTurn {
		availableSpells := s.availableSpells()
		for _, spell := range availableSpells {
			newSim := s.Clone()
			newSim.useSpell(spell)
			if *foundLeast == -1 || *foundLeast > newSim.manaUsed {
				if newSim.won() {
					*foundLeast = newSim.manaUsed
					*seq = newSim.spellsUsed
				} else if !newSim.lost() {
					findLeastMana(newSim, foundLeast, seq, hard)
				}
			}
		}
	} else {
		newSim := s.Clone()
		newSim.bossTurn()
		if newSim.lost() {
			return
		}
		findLeastMana(newSim, foundLeast, seq, hard)
	}
	return
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	bossHp, bossDamage := 0, 0
	for _, line := range lines {
		parts := strings.Split(line, ": ")
		name := parts[0]
		val := parts[1]
		num, err := strconv.Atoi(val)
		if err != nil {
			log.Fatalf("Failed parsing number '%s' %s", val, err)
		}
		switch name {
		case "Hit Points":
			bossHp = num
		case "Damage":
			bossDamage = num
		default:
			log.Fatalf("Unknown boss property '%s'", strings.TrimSuffix(name, ":"))
		}
	}

	spells := make(map[string]int)
	spells["Magic Missile"] = 53
	spells["Drain"] = 73
	spells["Shield"] = 113
	spells["Poison"] = 173
	spells["Recharge"] = 229

	const playerHp = 50
	const playerMana = 500
	switch part {
	case 1:
		s := NewSim(playerHp, playerMana, bossHp, bossDamage, spells)
		leastMana := -1
		sequence := make([]string, 0)
		findLeastMana(s, &leastMana, &sequence, false)
		log.Printf("Least mana that can be spent is %d", leastMana)
		log.Printf(`Winning strategy: "%s"`, strings.Join(sequence, `","`))
	case 2:
		s := NewSim(playerHp, playerMana, bossHp, bossDamage, spells)
		leastMana := -1
		sequence := make([]string, 0)
		findLeastMana(s, &leastMana, &sequence, true)
		log.Printf("Least mana that can be spent is %d", leastMana)
		log.Printf(`Winning strategy: "%s"`, strings.Join(sequence, `","`))
	}
}
