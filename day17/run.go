package day17

import (
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

func cloneContainers(containers []int, exclude int) []int {
	result := make([]int, len(containers))
	copy(result, containers)
	return result[exclude+1:]
}

func FindCombinations(containers []int, eggnog int) int {
	if len(containers) == 0 {
		if eggnog == 0 {
			return 1
		} else {
			return 0
		}
	}
	if eggnog < 0 {
		return 0
	}
	if eggnog == 0 {
		return 1
	}
	result := 0
	for i := 0; i < len(containers); i++ {
		newContainers := cloneContainers(containers, i)
		newEggnog := eggnog - containers[i]
		result += FindCombinations(newContainers, newEggnog)
	}
	return result
}

func FindShortestCombination(containers []int, eggnog int) []int {
	if len(containers) == 0 {
		if eggnog == 0 {
			return make([]int, 0)
		} else {
			return nil
		}
	}
	if eggnog < 0 {
		return nil
	}
	if eggnog == 0 {
		return make([]int, 0)
	}
	var shortest []int
	for i := 0; i < len(containers); i++ {
		newContainers := cloneContainers(containers, i)
		newEggnog := eggnog - containers[i]
		seq := FindShortestCombination(newContainers, newEggnog)
		if seq != nil {
			if shortest == nil || len(shortest) > len(seq) {
				shortest = append(seq, containers[i])
			}
		}
	}
	return shortest
}

func FindCombinationsLength(containers []int, eggnog int, length int) int {
	if length == 0 {
		if eggnog == 0 {
			return 1
		} else {
			return 0
		}
	}
	if len(containers) == 0 {
		if eggnog == 0 {
			return 1
		} else {
			return 0
		}
	}
	if eggnog < 0 {
		return 0
	}
	if eggnog == 0 {
		return 1
	}
	result := 0
	for i := 0; i < len(containers); i++ {
		newContainers := cloneContainers(containers, i)
		newEggnog := eggnog - containers[i]
		result += FindCombinationsLength(newContainers, newEggnog, length-1)
	}
	return result
}

func strArray(src []int) []string {
	result := make([]string, len(src))
	for i, n := range src {
		result[i] = strconv.Itoa(n)
	}
	return result
}

func Run(part int) {
	const eggnog = 150

	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	containers := make([]int, len(lines))
	for i, line := range lines {
		containers[i], err = strconv.Atoi(line)
		if err != nil {
			log.Fatalf("Failed parsing line '%s' (%s)", line, err)
		}
	}

	sort.Ints(containers)
	switch part {
	case 1:
		log.Printf("Found %d combinations to store %d liters of eggnog", FindCombinations(containers, eggnog), eggnog)
	case 2:
		shortest := FindShortestCombination(containers, eggnog)
		log.Printf("Shortest combination to store %d liters of eggnog is %s", eggnog, strings.Join(strArray(shortest), "+"))
		log.Printf("There are %d combinations of length %d", FindCombinationsLength(containers, eggnog, len(shortest)), len(shortest))
	}
}
