package day8

import (
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")

	switch part {
	case 1:
		stringCodeTotal := 0
		memoryTotal := 0
		for _, line := range lines {
			unquotedLine, err := strconv.Unquote(line)
			if err != nil {
				log.Fatalf("Failed parsing line %s - %s", line, err)
			}
			stringCodeTotal += len(line)
			memoryTotal += len(unquotedLine)
		}
		log.Printf("Processed %d lines", len(lines))
		log.Printf("Total strings code length is %d", stringCodeTotal)
		log.Printf("Total number of characters in memory is %d", memoryTotal)
		log.Printf("Answer is %d", stringCodeTotal-memoryTotal)
	case 2:
		stringCodeTotal := 0
		quotedTotal := 0
		for _, line := range lines {
			stringCodeTotal += len(line)
			quotedTotal += len(strconv.Quote(line))
		}
		log.Printf("Processed %d lines", len(lines))
		log.Printf("Total strings code length is %d", stringCodeTotal)
		log.Printf("Total quoted strings length is %d", quotedTotal)
		log.Printf("Answer is %d", quotedTotal-stringCodeTotal)
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
