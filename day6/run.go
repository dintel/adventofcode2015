package day6

import (
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

type field struct {
	cells [][]bool
}

func NewField(width, height int) *field {
	newField := &field{
		cells: make([][]bool, width),
	}
	for i := range newField.cells {
		newField.cells[i] = make([]bool, height)
	}
	return newField
}

func (f *field) TurnOn(fromX, fromY, toX, toY int) {
	for x := fromX; x <= toX; x++ {
		for y := fromY; y <= toY; y++ {
			f.cells[x][y] = true
		}
	}
}

func (f *field) TurnOff(fromX, fromY, toX, toY int) {
	for x := fromX; x <= toX; x++ {
		for y := fromY; y <= toY; y++ {
			f.cells[x][y] = false
		}
	}
}

func (f *field) Toggle(fromX, fromY, toX, toY int) {
	for x := fromX; x <= toX; x++ {
		for y := fromY; y <= toY; y++ {
			f.cells[x][y] = !f.cells[x][y]
		}
	}
}

func (f *field) Count(v bool) int {
	result := 0
	for x := 0; x < len(f.cells); x++ {
		for y := 0; y < len(f.cells[x]); y++ {
			if f.cells[x][y] == v {
				result++
			}
		}
	}
	return result
}

type numField struct {
	cells [][]int
}

func NewNumField(width, height int) *numField {
	newField := &numField{
		cells: make([][]int, width),
	}
	for i := range newField.cells {
		newField.cells[i] = make([]int, height)
	}
	return newField
}

func (f *numField) TurnOn(fromX, fromY, toX, toY int) {
	for x := fromX; x <= toX; x++ {
		for y := fromY; y <= toY; y++ {
			f.cells[x][y]++
		}
	}
}

func (f *numField) TurnOff(fromX, fromY, toX, toY int) {
	for x := fromX; x <= toX; x++ {
		for y := fromY; y <= toY; y++ {
			if f.cells[x][y] > 0 {
				f.cells[x][y]--
			}
		}
	}
}

func (f *numField) Toggle(fromX, fromY, toX, toY int) {
	for x := fromX; x <= toX; x++ {
		for y := fromY; y <= toY; y++ {
			f.cells[x][y] += 2
		}
	}
}

func (f *numField) Count() int {
	result := 0
	for x := 0; x < len(f.cells); x++ {
		for y := 0; y < len(f.cells[x]); y++ {
			result += f.cells[x][y]
		}
	}
	return result
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")

	switch part {
	case 1:
		field := NewField(1000, 1000)
		for i, line := range lines {
			parts := strings.Split(line, " ")
			startPoint := strings.Split(parts[len(parts)-3], ",")
			endPoint := strings.Split(parts[len(parts)-1], ",")

			if len(startPoint) != 2 {
				log.Printf("Failed parsing start coordinate %s at line %d", parts[len(parts)-3])
			}
			if len(endPoint) != 2 {
				log.Printf("Failed parsing end coordinate %s at line %d", parts[len(parts)-3])
			}

			startX, err := strconv.Atoi(startPoint[0])
			if err != nil {
				log.Printf("Failed parsing start coordinate %s at line %d", parts[len(parts)-3])
			}
			startY, err := strconv.Atoi(startPoint[1])
			if err != nil {
				log.Printf("Failed parsing start coordinate %s at line %d", parts[len(parts)-3])
			}
			endX, err := strconv.Atoi(endPoint[0])
			if err != nil {
				log.Printf("Failed parsing start coordinate %s at line %d", parts[len(parts)-1])
			}
			endY, err := strconv.Atoi(endPoint[1])
			if err != nil {
				log.Printf("Failed parsing start coordinate %s at line %d", parts[len(parts)-1])
			}

			switch parts[0] {
			case "turn":
				switch parts[1] {
				case "on":
					field.TurnOn(startX, startY, endX, endY)
				case "off":
					field.TurnOff(startX, startY, endX, endY)
				default:
					log.Printf("Unknown action %s %s at line %d", parts[0], parts[1], i)
				}
			case "toggle":
				field.Toggle(startX, startY, endX, endY)
			default:
				log.Printf("Unknown action %s at line %d", parts[0], i)
			}
		}
		log.Printf("Light on - %d", field.Count(true))
		log.Printf("Light off - %d", field.Count(false))
	case 2:
		field := NewNumField(1000, 1000)
		for i, line := range lines {
			parts := strings.Split(line, " ")
			startPoint := strings.Split(parts[len(parts)-3], ",")
			endPoint := strings.Split(parts[len(parts)-1], ",")

			if len(startPoint) != 2 {
				log.Printf("Failed parsing start coordinate %s at line %d", parts[len(parts)-3])
			}
			if len(endPoint) != 2 {
				log.Printf("Failed parsing end coordinate %s at line %d", parts[len(parts)-3])
			}

			startX, err := strconv.Atoi(startPoint[0])
			if err != nil {
				log.Printf("Failed parsing start coordinate %s at line %d", parts[len(parts)-3])
			}
			startY, err := strconv.Atoi(startPoint[1])
			if err != nil {
				log.Printf("Failed parsing start coordinate %s at line %d", parts[len(parts)-3])
			}
			endX, err := strconv.Atoi(endPoint[0])
			if err != nil {
				log.Printf("Failed parsing start coordinate %s at line %d", parts[len(parts)-1])
			}
			endY, err := strconv.Atoi(endPoint[1])
			if err != nil {
				log.Printf("Failed parsing start coordinate %s at line %d", parts[len(parts)-1])
			}

			switch parts[0] {
			case "turn":
				switch parts[1] {
				case "on":
					field.TurnOn(startX, startY, endX, endY)
				case "off":
					field.TurnOff(startX, startY, endX, endY)
				default:
					log.Printf("Unknown action %s %s at line %d", parts[0], parts[1], i)
				}
			case "toggle":
				field.Toggle(startX, startY, endX, endY)
			default:
				log.Printf("Unknown action %s at line %d", parts[0], i)
			}
		}
		log.Printf("Total light power of grid is %d", field.Count())
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
