package day2

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

type box struct {
	l, w, h int
}

func parseBox(s string) (*box, error) {
	parts := strings.Split(s, "x")
	if len(parts) != 3 {
		return nil, errors.New(fmt.Sprintf("Failed parsing box - %s", s))
	}
	var err error
	result := new(box)
	result.l, err = strconv.Atoi(parts[0])
	if err != nil {
		return nil, err
	}
	result.w, err = strconv.Atoi(parts[1])
	if err != nil {
		return nil, err
	}
	result.h, err = strconv.Atoi(parts[2])
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (box box) Surface() int {
	return 2*box.l*box.w + 2*box.w*box.h + 2*box.h*box.l
}

func (box box) Extra() int {
	dims := []int{box.h, box.l, box.w}
	sort.Ints(dims)
	return dims[0] * dims[1]
}

func (box box) Ribbon() int {
	dims := []int{box.h, box.l, box.w}
	sort.Ints(dims)
	return dims[0] + dims[0] + dims[1] + dims[1] + dims[0]*dims[1]*dims[2]
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	boxes := make([]*box, 0)
	for _, line := range lines {
		box, err := parseBox(line)
		if err != nil {
			log.Fatal(err)
		}
		boxes = append(boxes, box)
	}
	log.Printf("Parsed %d boxes", len(boxes))

	switch part {
	case 1:
		paper := 0
		for _, box := range boxes {
			paper += box.Surface() + box.Extra()
		}
		log.Printf("Total paper needed: %d", paper)
	case 2:
		ribbon := 0
		for _, box := range boxes {
			ribbon += box.Ribbon()
		}
		log.Printf("Total ribbon needed: %d", ribbon)
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
