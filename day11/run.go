package day11

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func nextPassword(oldPassword string) string {
	newPassword := []rune(oldPassword)
	carry := true
	i := len(newPassword) - 1
	for carry {
		if i < 0 {
			break
		}
		newPassword[i]++
		if newPassword[i] > rune('z') {
			newPassword[i] = 'a'
			i--
		} else {
			carry = false
		}
	}
	if carry {
		return fmt.Sprintf("z%s", string(newPassword))
	}
	return string(newPassword)
}

func HasStraight(str string, length int) bool {
	if length > len(str) {
		return false
	}
	for i := 0; i < len(str)-length+1; i++ {
		found := true
		for j := 0; j < length-1; j++ {
			if str[i+j] != str[i+j+1]-1 {
				found = false
				break
			}
		}
		if found {
			return true
		}
	}
	return false
}

func ContainsLetter(str string, letters []rune) bool {
	for _, ch := range str {
		for _, letter := range letters {
			if ch == letter {
				return true
			}
		}
	}
	return false
}

func HasPairs(str string, max int) bool {
	pairs := 0
	for i := 0; i < len(str)-1; i++ {
		if str[i] == str[i+1] {
			i++
			pairs++
			if pairs == max {
				return true
			}
		}
	}
	return false
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	oldPassword := string(data)

	switch part {
	case 1:
		log.Printf("Old password: %s", oldPassword)
		newPassword := nextPassword(oldPassword)
		for !(HasStraight(newPassword, 3) && !ContainsLetter(newPassword, []rune{'i', 'o', 'l'}) && HasPairs(newPassword, 2)) {
			newPassword = nextPassword(newPassword)
		}
		log.Printf("New password: %s", newPassword)
	case 2:
		log.Printf("Old password: %s", oldPassword)
		newPassword := nextPassword(oldPassword)
		for !(HasStraight(newPassword, 3) && !ContainsLetter(newPassword, []rune{'i', 'o', 'l'}) && HasPairs(newPassword, 2)) {
			newPassword = nextPassword(newPassword)
		}
		newPassword = nextPassword(newPassword)
		for !(HasStraight(newPassword, 3) && !ContainsLetter(newPassword, []rune{'i', 'o', 'l'}) && HasPairs(newPassword, 2)) {
			newPassword = nextPassword(newPassword)
		}
		log.Printf("New password: %s", newPassword)
	default:
		log.Fatalf("Unknown part %d in day 10", part)
	}
}
