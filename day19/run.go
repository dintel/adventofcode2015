package day19

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
)

type rule struct {
	from string
	to   string
}

func (r *rule) String() string {
	return fmt.Sprintf("%s -> %s", r.from, r.to)
}

func (r *rule) Reverse() *rule {
	return &rule{
		from: r.to,
		to:   r.from,
	}
}

func ruleFromLine(line string) (*rule, error) {
	from, to := "", ""
	_, err := fmt.Sscanf(line, "%s => %s", &from, &to)
	if err != nil {
		return nil, err
	}
	r := &rule{
		from: from,
		to:   to,
	}
	return r, nil
}

func replace(s string, r *rule) []string {
	parts := strings.Split(s, r.from)
	result := make([]string, len(parts)-1)
	for i := 0; i < len(parts)-1; i++ {
		replacement := fmt.Sprintf("%s%s%s", parts[i], r.to, parts[i+1])
		prefix := strings.Join(parts[:i], r.from)
		suffix := strings.Join(parts[i+2:], r.from)
		if len(prefix) != 0 {
			prefix = prefix + r.from
		}
		if len(suffix) != 0 {
			suffix = r.from + suffix
		}
		result[i] = fmt.Sprintf("%s%s%s", prefix, replacement, suffix)
	}
	return result
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	medicine := lines[len(lines)-1]
	lines = lines[:len(lines)-2]
	rules := make([]*rule, len(lines))
	for i, line := range lines {
		rules[i], err = ruleFromLine(line)
		if err != nil {
			log.Fatalf("Failed parsing rule '%s' - %s", line, err)
		}
	}
	log.Printf("Parsed %d rules", len(rules))
	log.Printf("Medicine string is %d characters long", len(medicine))

	switch part {
	case 1:
		calibrationMap := make(map[string]bool)
		for _, rule := range rules {
			calibrationStrings := replace(medicine, rule)
			for _, s := range calibrationStrings {
				calibrationMap[s] = true
			}
		}
		log.Printf("Calibration done - %d calibration strings", len(calibrationMap))
	case 2:
		rRules := make([]*rule, len(rules))
		for i, r := range rules {
			rRules[i] = r.Reverse()
		}
		current := medicine
		i := 0
		for current != "e" {
			choices := make([]string, 0)
			for _, rule := range rRules {
				choices = append(choices, replace(current, rule)...)
			}
			sort.Strings(choices)
			current = choices[len(choices)-1]
			i++
		}
		log.Printf("Minimum number of steps to create medicine is %d", i)
	}
}
