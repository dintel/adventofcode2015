package day16

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

type mfcsam struct {
	Children    int
	Cats        int
	Samoyeds    int
	Pomeranians int
	Akitas      int
	Vizslas     int
	Goldfish    int
	Trees       int
	Cars        int
	Perfumes    int
}

type aunt struct {
	Name        string
	Children    *int
	Cats        *int
	Samoyeds    *int
	Pomeranians *int
	Akitas      *int
	Vizslas     *int
	Goldfish    *int
	Trees       *int
	Cars        *int
	Perfumes    *int
}

func (a aunt) String() string {
	return fmt.Sprintf("%s - Children:%d Cats:%d Samoyeds:%d Pomeranians:%d Akitas:%d Vizslas:%d Goldfish:%d Trees:%d Cars:%d Perfumes:%d", a.Name, a.Children, a.Cats, a.Samoyeds, a.Pomeranians, a.Akitas, a.Vizslas, a.Goldfish, a.Trees, a.Cars, a.Perfumes)
}

func matches(known mfcsam, a aunt) bool {
	if a.Children != nil && *a.Children != known.Children {
		return false
	}
	if a.Cats != nil && *a.Cats != known.Cats {
		return false
	}
	if a.Samoyeds != nil && *a.Samoyeds != known.Samoyeds {
		return false
	}
	if a.Pomeranians != nil && *a.Pomeranians != known.Pomeranians {
		return false
	}
	if a.Akitas != nil && *a.Akitas != known.Akitas {
		return false
	}
	if a.Vizslas != nil && *a.Vizslas != known.Vizslas {
		return false
	}
	if a.Goldfish != nil && *a.Goldfish != known.Goldfish {
		return false
	}
	if a.Trees != nil && *a.Trees != known.Trees {
		return false
	}
	if a.Cars != nil && *a.Cars != known.Cars {
		return false
	}
	if a.Perfumes != nil && *a.Perfumes != known.Perfumes {
		return false
	}
	return true
}

func matchesFixed(known mfcsam, a aunt) bool {
	if a.Children != nil && *a.Children != known.Children {
		return false
	}
	if a.Cats != nil && *a.Cats <= known.Cats {
		return false
	}
	if a.Samoyeds != nil && *a.Samoyeds != known.Samoyeds {
		return false
	}
	if a.Pomeranians != nil && *a.Pomeranians >= known.Pomeranians {
		return false
	}
	if a.Akitas != nil && *a.Akitas != known.Akitas {
		return false
	}
	if a.Vizslas != nil && *a.Vizslas != known.Vizslas {
		return false
	}
	if a.Goldfish != nil && *a.Goldfish >= known.Goldfish {
		return false
	}
	if a.Trees != nil && *a.Trees <= known.Trees {
		return false
	}
	if a.Cars != nil && *a.Cars != known.Cars {
		return false
	}
	if a.Perfumes != nil && *a.Perfumes != known.Perfumes {
		return false
	}
	return true
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	aunts := make([]aunt, len(lines))
	for i, line := range lines {
		tokens := strings.Split(line, " ")
		aunts[i].Name = fmt.Sprintf("%s %s", tokens[0], strings.Trim(tokens[1], ":"))
		tokens = tokens[2:]
		for j := 0; j < len(tokens); j += 2 {
			name := strings.Trim(tokens[j], ":")
			number, err := strconv.Atoi(strings.Trim(tokens[j+1], ","))
			if err != nil {
				log.Fatalf("Failed parsing line: %s", line)
			}
			pNumber := new(int)
			*pNumber = number
			switch name {
			case "children":
				aunts[i].Children = pNumber
			case "cats":
				aunts[i].Cats = pNumber
			case "samoyeds":
				aunts[i].Samoyeds = pNumber
			case "pomeranians":
				aunts[i].Pomeranians = pNumber
			case "akitas":
				aunts[i].Akitas = pNumber
			case "vizslas":
				aunts[i].Vizslas = pNumber
			case "goldfish":
				aunts[i].Goldfish = pNumber
			case "trees":
				aunts[i].Trees = pNumber
			case "cars":
				aunts[i].Cars = pNumber
			case "perfumes":
				aunts[i].Perfumes = pNumber
			default:
				log.Fatalf("Failed parsing line: %s", line)
			}
		}
	}

	known := mfcsam{
		Children:    3,
		Cats:        7,
		Samoyeds:    2,
		Pomeranians: 3,
		Akitas:      0,
		Vizslas:     0,
		Goldfish:    5,
		Trees:       3,
		Cars:        2,
		Perfumes:    1,
	}

	switch part {
	case 1:
		for _, aunt := range aunts {
			if matches(known, aunt) {
				log.Printf("Matching aunt - %s", aunt)
			}
		}
	case 2:
		for _, aunt := range aunts {
			if matchesFixed(known, aunt) {
				log.Printf("Matching aunt - %s", aunt)
			}
		}
	default:
		log.Fatalf("Unknown part %d in day 15", part)
	}
}
