package day5

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func Has3Vowels(s string) bool {
	vowels := 0
	for _, c := range s {
		if c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' {
			vowels++
		}
	}
	return vowels > 2
}

func HasDoubleLetter(s string) bool {
	for i := 0; i < len(s)-1; i++ {
		if s[i] == s[i+1] {
			return true
		}
	}
	return false
}

func HasNotNaughtySubstring(s string) bool {
	ab := !strings.Contains(s, "ab")
	cd := !strings.Contains(s, "cd")
	pq := !strings.Contains(s, "pq")
	xy := !strings.Contains(s, "xy")
	return ab && cd && pq && xy
}

func HasDoublePair(s string) bool {
	for i := 0; i < len(s)-3; i++ {
		pair := s[i : i+2]
		if strings.Contains(s[i+2:], pair) {
			return true
		}
	}
	return false
}

func HasDoubleLetterWithOneLetterBetween(s string) bool {
	for i := 0; i < len(s)-2; i++ {
		if s[i] == s[i+2] {
			return true
		}
	}
	return false
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")

	switch part {
	case 1:
		naughty, nice := 0, 0
		for _, str := range lines {
			if Has3Vowels(str) && HasDoubleLetter(str) && HasNotNaughtySubstring(str) {
				nice++
			} else {
				naughty++
			}
		}
		log.Printf("There are %d nice strings and %d naughty strings", nice, naughty)
	case 2:
		naughty, nice := 0, 0
		for _, str := range lines {
			if HasDoublePair(str) && HasDoubleLetterWithOneLetterBetween(str) {
				nice++
			} else {
				naughty++
			}
		}
		log.Printf("There are %d nice strings and %d naughty strings", nice, naughty)
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
