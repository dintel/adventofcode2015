package day10

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sync"
)

func lookAndSay(s string) string {
	result := ""
	prev := '.'
	sameCount := 0
	for _, c := range s {
		if prev == '.' {
			prev = c
			sameCount = 1
			continue
		}
		if prev == c {
			sameCount++
		} else {
			result += fmt.Sprintf("%d%c", sameCount, prev)
			prev = c
			sameCount = 1
		}
	}
	return fmt.Sprintf("%s%d%c", result, sameCount, prev)
}

func lookAndSayProcessor(in, out chan string, n int) {
	prev := '.'
	sameCount := 0
	for str := range in {
		for _, c := range str {
			if prev == '.' {
				prev = c
				sameCount = 1
				continue
			}
			if prev == c {
				sameCount++
			} else {
				out <- fmt.Sprintf("%d%c", sameCount, prev)
				prev = c
				sameCount = 1
			}
		}
	}
	out <- fmt.Sprintf("%d%c", sameCount, prev)
	close(out)
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	initialString := string(data)

	switch part {
	case 1:
		result := initialString
		for i := 0; i < 40; i++ {
			result = lookAndSay(result)
		}
		log.Printf("Result: %s", result)
		log.Printf("Result length is %d", len(result))
	case 2:
		steps := 50
		chans := make([]chan string, steps+1)
		var wg sync.WaitGroup
		wg.Add(steps + 1)
		chans[0] = make(chan string, 10)
		for i := 0; i < steps; i++ {
			chans[i+1] = make(chan string, 10)
			go func(in, out chan string, n int) {
				defer wg.Done()
				lookAndSayProcessor(in, out, n)
				log.Printf("Processor %d finished", n)
			}(chans[i], chans[i+1], i)
		}
		result := ""
		go func() {
			defer wg.Done()
			for str := range chans[steps] {
				result += str
			}
		}()
		for _, ch := range initialString {
			chans[0] <- string(ch)
		}
		close(chans[0])
		wg.Wait()
		log.Printf("Result: %s", result)
		log.Printf("Result length is %d", len(result))
	default:
		log.Fatalf("Unknown part %d in day 10", part)
	}
}
