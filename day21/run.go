package day21

import (
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

type item struct {
	name   string
	cost   int
	damage int
	armor  int
}

func NewItem(name string, cost, damage, armor int) *item {
	return &item{
		name:   name,
		cost:   cost,
		damage: damage,
		armor:  armor,
	}
}

type boss struct {
	hp     int
	damage int
	armor  int
}

type player struct {
	hp     int
	damage int
	armor  int
	cost   int
}

func NewPlayer(hp int, weapon *item, armor *item, ring1 *item, ring2 *item) player {
	pDamage := 0
	pArmor := 0
	pCost := 0
	items := []*item{weapon, armor, ring1, ring2}
	for _, item := range items {
		if item != nil {
			pDamage += item.damage
			pArmor += item.armor
			pCost += item.cost
		}
	}
	return player{
		hp:     hp,
		damage: pDamage,
		armor:  pArmor,
		cost:   pCost,
	}
}

func simulate(b boss, p player) bool {
	playerTurn := true
	playerTurnDamage := p.damage - b.armor
	if playerTurnDamage <= 0 {
		playerTurnDamage = 1
	}
	bossTurnDamage := b.damage - p.armor
	if bossTurnDamage <= 0 {
		bossTurnDamage = 1
	}
	for b.hp > 0 && p.hp > 0 {
		if playerTurn {
			b.hp -= playerTurnDamage
		} else {
			p.hp -= bossTurnDamage
		}
		playerTurn = !playerTurn
	}
	return p.hp > 0
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	boss := boss{}
	for _, line := range lines {
		parts := strings.Split(line, ": ")
		name := parts[0]
		val := parts[1]
		num, err := strconv.Atoi(val)
		if err != nil {
			log.Fatalf("Failed parsing number '%s' %s", val, err)
		}
		switch name {
		case "Hit Points":
			boss.hp = num
		case "Damage":
			boss.damage = num
		case "Armor":
			boss.armor = num
		default:
			log.Fatalf("Unknown boss property '%s'", strings.TrimSuffix(name, ":"))
		}
	}

	weapons := make([]*item, 5)
	weapons[0] = NewItem("Dagger", 8, 4, 0)
	weapons[1] = NewItem("Shortsword", 10, 5, 0)
	weapons[2] = NewItem("Warhammer", 25, 6, 0)
	weapons[3] = NewItem("Longsword", 40, 7, 0)
	weapons[4] = NewItem("Greataxe", 74, 8, 0)

	armors := make([]*item, 6)
	armors[0] = NewItem("Leather", 13, 0, 1)
	armors[1] = NewItem("Chainmail", 31, 0, 2)
	armors[2] = NewItem("Splintmail", 53, 0, 3)
	armors[3] = NewItem("Bandedmail", 75, 0, 4)
	armors[4] = NewItem("Platemail", 102, 0, 5)

	rings := make([]*item, 7)
	rings[0] = NewItem("Damage +1", 25, 1, 0)
	rings[1] = NewItem("Damage +2", 50, 2, 0)
	rings[2] = NewItem("Damage +3", 100, 3, 0)
	rings[3] = NewItem("Defense +1", 20, 0, 1)
	rings[4] = NewItem("Defense +2", 40, 0, 2)
	rings[5] = NewItem("Defense +3", 80, 0, 3)

	const playerHp = 100
	switch part {
	case 1:
		bestCost := -1
		for _, w := range weapons {
			for _, a := range armors {
				for _, r1 := range rings {
					for _, r2 := range rings {
						if r1 == r2 && r1 != nil {
							continue
						}
						p := NewPlayer(playerHp, w, a, r1, r2)
						if simulate(boss, p) {
							if bestCost == -1 {
								bestCost = p.cost
							}
							if p.cost < bestCost {
								bestCost = p.cost
							}
						}
					}
				}
			}
		}
		log.Printf("Least amount of gold to spend and win is %d", bestCost)
	case 2:
		bestCost := -1
		for _, w := range weapons {
			for _, a := range armors {
				for _, r1 := range rings {
					for _, r2 := range rings {
						if r1 == r2 && r1 != nil {
							continue
						}
						p := NewPlayer(playerHp, w, a, r1, r2)
						if !simulate(boss, p) {
							if bestCost == -1 {
								bestCost = p.cost
							}
							if p.cost > bestCost {
								bestCost = p.cost
							}
						}
					}
				}
			}
		}
		log.Printf("Most amount of gold to spend and loose is %d", bestCost)
	}
}
