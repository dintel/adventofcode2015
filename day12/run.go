package day12

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func sum(f interface{}) (output float64) {
outer:
	switch fv := f.(type) {
	case []interface{}:
		for _, v := range fv {
			output += sum(v)
		}
	case float64:
		output += fv
	case map[string]interface{}:
		for _, v := range fv {
			if v == "red" {
				break outer
			}
		}
		for _, v := range fv {
			output += sum(v)
		}
	}
	return
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}

	switch part {
	case 1:
		stripUnneeded := regexp.MustCompile(`["\[\]:a-z,{}]+`)
		numbersStr := strings.Split(strings.Trim(stripUnneeded.ReplaceAllString(string(data), " "), " "), " ")
		result := 0
		for _, numberStr := range numbersStr {
			number, err := strconv.Atoi(numberStr)
			if err != nil {
				log.Fatalf("Failed parsing %s as number", numberStr)
			}
			result += number
		}
		log.Printf("Result: %d", result)
	case 2:
		var f interface{}
		json.Unmarshal(data, &f)
		log.Printf("Result: %d", int64(sum(f)))
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
