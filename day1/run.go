package day1

import (
	"io/ioutil"
	"log"
	"os"
)

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}

	switch part {
	case 1:
		floor := 0
		for _, c := range data {
			switch c {
			case '(':
				floor++
			case ')':
				floor--
			default:
				log.Printf("Unknown character '%s', skipping", string(c))
			}

		}
		log.Printf("Result floor: %d\n", floor)
	case 2:
		floor := 0
		first := -1
		for i, c := range data {
			switch c {
			case '(':
				floor++
			case ')':
				floor--
			default:
				log.Printf("Unknown character '%s', skipping", string(c))
			}
			if floor < 0 {
				first = i
				break
			}

		}
		log.Printf("Result character: %d\n", first+1)
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
