package day15

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type ingridient struct {
	Name       string
	Capacity   int
	Durability int
	Flavor     int
	Texture    int
	Calories   int
}

func (i *ingridient) String() string {
	return fmt.Sprintf("%s - Capacity %d, Durability %d, Flavor %d, Texture %d, Calories %d", i.Name, i.Capacity, i.Durability, i.Flavor, i.Texture, i.Calories)
}

func score(ingridients []*ingridient, ratio []int) int {
	capacity, durability, flavor, texture := 0, 0, 0, 0
	for i, spoons := range ratio {
		capacity += ingridients[i].Capacity * spoons
		durability += ingridients[i].Durability * spoons
		flavor += ingridients[i].Flavor * spoons
		texture += ingridients[i].Texture * spoons
	}
	if capacity < 0 {
		capacity = 0
	}
	if durability < 0 {
		durability = 0
	}
	if flavor < 0 {
		flavor = 0
	}
	if texture < 0 {
		texture = 0
	}
	return capacity * durability * flavor * texture
}

func calories(ingridients []*ingridient, ratio []int) int {
	c := 0
	for i, spoons := range ratio {
		c += spoons * ingridients[i].Calories
	}
	return c
}

func spoons(ratio []int) int {
	result := 0
	for _, spoons := range ratio {
		result += spoons
	}
	return result
}

func nextChoice(choice []int, max int) ([]int, bool) {
	overflow := true
	choice[0]++
	for i := 0; i < len(choice); i++ {
		localOverflow := false
		if choice[i] > max {
			choice[i] = 0
			localOverflow = true
		}
		if localOverflow && i < len(choice)-1 {
			choice[i+1]++
		}
		if choice[i] != 0 {
			overflow = false
		}
	}
	return choice, overflow
}

func findBestScore(ingridients []*ingridient, max int) int {
	choice := make([]int, len(ingridients))
	overflow := false
	bestScore := 0
	for !overflow {
		choice, overflow = nextChoice(choice, max)
		if spoons(choice) == max {
			score := score(ingridients, choice)
			if score > bestScore {
				bestScore = score
			}
		}
	}
	return bestScore
}

func findBestScoreCalories(ingridients []*ingridient, max int, c int) int {
	choice := make([]int, len(ingridients))
	overflow := false
	bestScore := 0
	for !overflow {
		choice, overflow = nextChoice(choice, max)
		if spoons(choice) == max && calories(ingridients, choice) == c {
			score := score(ingridients, choice)
			if score > bestScore {
				bestScore = score
			}
		}
	}
	return bestScore
}

func Run(part int) {
	if len(os.Args) != 4 {
		log.Fatalf("Expected input file parameter")
	}
	filename := os.Args[3]
	log.Printf("Loading file %s", filename)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to read input file - %s", err)
	}
	lines := strings.Split(string(data), "\n")
	ingridients := make([]*ingridient, len(lines))
	for i, line := range lines {
		var name string
		var capacity, durability, flavor, texture, calories int
		_, err := fmt.Sscanf(line, "%s capacity %d, durability %d, flavor %d, texture %d, calories %d", &name, &capacity, &durability, &flavor, &texture, &calories)
		if err != nil {
			log.Fatalf("Failed parsing line '%s' (%s)", line, err)
		}
		ingridients[i] = &ingridient{
			Name:       name,
			Capacity:   capacity,
			Durability: durability,
			Flavor:     flavor,
			Texture:    texture,
			Calories:   calories,
		}
	}

	switch part {
	case 1:
		for i, ing := range ingridients {
			log.Printf("%d %s", i, ing)
		}
		log.Printf("Best cookie score %d", findBestScore(ingridients, 100))
	case 2:
		for i, ing := range ingridients {
			log.Printf("%d %s", i, ing)
		}
		log.Printf("Best cookie score %d", findBestScoreCalories(ingridients, 100, 500))
	default:
		log.Fatalf("Unknown part %d in day 1", part)
	}
}
